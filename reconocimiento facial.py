import numpy as np
import cv2
import argparse

  


face_cascade = cv2.CascadeClassifier('C:\\Users\\JAIME PINEDA\\Desktop\\python programas\\tarea de proyectos\\haarcascade_frontalcatface.xml')
cap = cv2.VideoCapture(0)
  
while(True):
    #Leemos un frame y lo guardamos
    valido, img = cap.read()

    if valido:
        

        img_gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        array_rostros = face_cascade.detectMultiScale(img_gris, 1.3, 5)
        blur=cv2.GaussianBlur(img_gris, (11,11),0)
        #edge =    (blur,dencidad de lineas)
        edged = cv2.Canny(blur, 30, 30)
    
        for (x,y,w,h) in array_rostros:
                cv2.rectangle(img,(x,y),(x+w,y+h),(125,255,0),2)

                
        (cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        #Mostramos la imagen
        cv2.imshow('img',img)
        cv2.imshow('Edges',edged)
        
          
        #Con la tecla 'q' salimos del programa
        if cv2.waitKey(1) & 0xFF == ord('q'):
                break
cap.release()
cv2.destroyAllWindows()
